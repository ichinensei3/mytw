class Tweet < ApplicationRecord
    #belongs_to :tweet
    validates :message, presence: true, length: { maximum: 150 }
end
